using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public Text Clicks;
    float clicksTotals;
    public AudioSource sound1;
    public AudioSource sound2;
    public GameObject panel;
    public GameObject signInPanel;
    public GameObject logInPanel;

    public void ClicksAdd()
    {
        if (BoostScript.boostActivated2)
        {
            clicksTotals += 5;
            Clicks.text = clicksTotals.ToString("0");
        }
        else
        {
            clicksTotals++;
            Clicks.text = clicksTotals.ToString("0");
        }
    }

    public void ClicksBoost()
    {
        clicksTotals += 150;
        Clicks.text = clicksTotals.ToString("0");
    }

    public void SoundBoost()
    {
        sound1.Play();
    }

    public void SoundBoost2()
    {
        sound2.Play();
    }

    public void Pause()
    {
        panel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Unpause()
    {
        panel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void SignInButton()
    {
        logInPanel.SetActive(false);
        signInPanel.SetActive(true);
    }

    public void LogIn()
    {
        signInPanel.SetActive(false);
        logInPanel.SetActive(true);
      
    }

    public void OnClickLogInButton()
    {
        SceneManager.LoadScene(1);
    }

    public void OnRankingButton()
    {
        SceneManager.LoadScene(2);
    }

    public void OnButtonBack (){
        SceneManager.LoadScene(1);  
    }
}

