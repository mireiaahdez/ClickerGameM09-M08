using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class UserForm
{
    public string userName { get; set; }
    public string password { get; set; }
}
public class LogInController : MonoBehaviour
{
    public InputField UsernameField;
    public InputField PasswordField;
    public InputField UsernameFieldNew;
    public InputField PasswordFieldNew;
    private HttpClient httpClient = new HttpClient();
    private object encoding;

    public async void SignIn()
    {
        if (UsernameField.text == null || PasswordField.text == null)
        {
            Debug.Log("Please enter both username and password");
            return;
        }

        var signInUser = new UserForm();
        signInUser.userName = UsernameFieldNew.text.ToLower();
        signInUser.password = PasswordFieldNew.text.ToLower();

        string json = "{\"userName\":\"" + signInUser.userName + "\",\"password\":\"" + signInUser.password + "\"}";
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await httpClient.PostAsync("http://localhost:8080/api/users/register", content);

        if (response.IsSuccessStatusCode)
        {
            Debug.Log(response.Content.ReadAsStringAsync().Result);
            PlayerPrefs.SetString("userName", UsernameField.text);
        }
        else
        {
            Debug.Log(response.Content.ReadAsStringAsync().Result);
            return;
        }
    }
    
    public async void LogIn()
    {
        if(string.IsNullOrEmpty(UsernameField.text) || string.IsNullOrEmpty(PasswordField.text))
        {
            Debug.Log("Please enter both username and password");
            return;
        }

        var logInUser = new UserForm();
        logInUser.userName = UsernameField.text.ToLower();
        logInUser.password = PasswordField.text.ToLower();

        string json = "{\"userName\":\"" + logInUser.userName + "\",\"password\":\"" + logInUser.password + "\"}";
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await httpClient.PostAsync("http://localhost:8080/api/users/login", content);

        if (response.IsSuccessStatusCode)
        {
            Debug.Log(response.Content.ReadAsStringAsync().Result);
            PlayerPrefs.SetString("userName", UsernameField.text);
            SceneManager.LoadScene(1);
        }
        else
        {
            Debug.Log(response.Content.ReadAsStringAsync().Result);
            return;
        }
    }
}
