using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using UnityEngine;

public class RankingController : MonoBehaviour
{
    
    private const string SERVER_URL = "http://localhost:5000/ranking"; // URL del endpoint del servidor
    private HttpClient client;

    private int highScore; // Puntaje m�s alto registrado en el juego
    private int playerScore; // Puntaje actual del jugador

    private void Start()
    {
        client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Solicita los datos del ranking al servidor
        //StartCoroutine(GetRanking());
    }

    /*private IEnumerator GetRanking()
    {
        HttpResponseMessage response = await client.GetAsync(SERVER_URL);
        response.EnsureSuccessStatusCode();
        string responseBody = await response.Content.ReadAsStringAsync();
        RankData rankData = JsonUtility.FromJson<RankData>(responseBody);

        // Almacena los datos del ranking en variables locales
        highScore = rankData.highScore;
        playerScore = rankData.playerScore;
    }*/

    public void UpdateRanking(int newScore)
    {
        // Actualiza los datos del ranking
        if (newScore > highScore)
        {
            highScore = newScore;
        }
        playerScore = newScore;

        // Env�a los nuevos datos del ranking al servidor
        StartCoroutine(PostRanking());
    }

    private IEnumerator PostRanking()
    {
        RankData rankData = new RankData(highScore, playerScore);
        string jsonRankData = JsonUtility.ToJson(rankData);

        var content = new StringContent(jsonRankData, Encoding.UTF8, "application/json");
        //HttpResponseMessage response = await client.PostAsync(SERVER_URL, content);
        //response.EnsureSuccessStatusCode();

        yield return null;
    }
}

// Clase auxiliar para almacenar los datos del ranking
[System.Serializable]
public class RankData
{
    public int highScore;
    public int playerScore;

    public RankData (int highscore, int playerscore)
    {
        highScore = highscore;
        playerScore = playerscore;

    }
}