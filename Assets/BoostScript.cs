using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostScript : MonoBehaviour
{
    public GameObject imageBoost;
    public GameObject parentTransform;
    public Button boostButton;
    public Button boostButton2;
    private float spawnTimer;
    private float spawnInterval2;
    private float boostTime1 = 7f;
    public static bool boostActivated2 = false;
    private float boostTime2 = 5f;
    public Animator animator;
    public Animator animator2;

    // Start is called before the first frame update
    void Start()
    {
        parentTransform = GameObject.Find("Panel");
        spawnInterval2 = 0.1f;
        boostButton.onClick.AddListener(ActivateBoost1);
        boostButton2.onClick.AddListener(ActivateBoost2);
    }

    // Update is called once per frame
    void Update()
    {
        if (boostActivated2)
        {
            //Durante el tiempo de boost
            spawnTimer += Time.deltaTime;

            if (spawnTimer >= spawnInterval2)
            {
                spawnTimer = 0f;
                GameObject newImage = Instantiate(imageBoost, new Vector3(Random.Range(transform.position.x - 350f, transform.position.x + 350f), -760f, 0f), Quaternion.identity, parentTransform.transform);
                newImage.AddComponent<MoveUp>();
                //MoveUp moveUp = newImage.GetComponent<MoveUp>();
            }

        }
    }

    public void ActivateBoost1()
    {
        boostButton.interactable = false;
        Invoke("DesactivateBoost1", boostTime1);
    }

    void DesactivateBoost1()
    {
        boostButton.interactable = true;
    }

    public void ActivateBoost2()
    {
        if (!boostActivated2)
        {
            boostButton2.interactable = false;
            boostActivated2 = true;
            animator.speed = 3.0f;
            animator2.speed = 3.0f;
            Invoke("DesactivateBoost2", boostTime2);
        }
    }

    void DesactivateBoost2()
    {
        boostActivated2 = false;
        animator.speed = 1.0f;
        animator2.speed = 1.0f;
        StartCoroutine(ExampleCoroutine());
    }

    IEnumerator ExampleCoroutine()
    {
        yield return new WaitForSeconds(10);

        boostButton2.interactable = true;
    }
}
