using UnityEngine;

public class MoveUp : MonoBehaviour
{
    public float speed = 790f; // velocidad de movimiento hacia arriba
    public float lifetime = 2.2f; // tiempo de vida del objeto en segundos

    private float timer = 0f; // contador de tiempo

    void Update()
    {
        // mueve el objeto hacia arriba
        transform.Translate(Vector3.up * speed * Time.deltaTime);

        // actualiza el contador de tiempo
        timer += Time.deltaTime;

        // destruye el objeto despu�s de cierto tiempo
        if (timer >= lifetime)
        {
            Destroy(gameObject);
        }
    }
}
