import express from "express";
import bodyParser from "body-parser";
import { Server } from "socket.io";

const app = express()
/*const io = require('socket.io')(8080)

io.on('connection', (socket) => {
    console.log('Un usuario se ha conectado');
    
    socket.on('mensaje', (data) => {
        console.log('Mensaje recibido: ${data}');
        io.emit('mensaje', data); // envía el mensaje a todos los clientes conectados
      });
      
      socket.on('disconnect', () => {
        console.log('Un usuari se ha desconectado');
      });
    });*/

// parse application/json
app.use(bodyParser.json())

app.listen(process.env.PORT || 8080);

const players = [
    { userName: 'admin', score: 5, password: 'password' },
    { userName: 'noe', score: 5, password: 'password' },
    { userName: 'alejandro', score: 3, password: 'password' },
    { userName: 'mireia', score: 8, password: 'password' }
]

app.use(bodyParser.urlencoded({ extended: true }))

app.get('/api/users', (req, res) => {
    //Consultar tots els usuaris
    res.send(players)
})

app.get('/api/users/:UserName', (req, res) => {
    //Consultar les dades d’un usuari donat un nom

    var consulta = players.find(a => a.userName === req.params.UserName)

    if (consulta === undefined) {
        res.send("No se ha encontrado este usuario");
    } else {
        const player = { userName: consulta.userName, score: consulta.score }
        res.send(player);
    }
})

app.post('/api/users/register', (req, res) => {
    //Afegir un usuari donant el nom i contrasenya
    var user = { username: req.body.userName, score: 0, password: req.body.password }
    var consulta = players.find(a => a.userName === user.userName)

    if (consulta != null) {
        res.send("Aquest usuari ja existeix!")
        console.log("1")
    }
    else {
        players.push(user)
        res.send("Usuari agregat exitosament.");
        console.log("2")
    }
})


app.post('/api/users/login', (req, res) => {
    //Login

    const pedida = { username: req.body.userName, password: req.body.password }
    const user = players.find(user => user.userName === pedida.username && user.password === pedida.password);

    if (user) {
        // aquí podrías iniciar sesión usando alguna forma de autenticación
        res.status(200).send('Inicio de sesión exitoso');
    } else {
        res.status(401).send('Credenciales inválidas');
    }
});

app.put('/api/users/changeUsername/:UserName', (req, res) => {
    //Canviar el nom d’un usuari donat el nom
    var consulta = players.find(a => a.userName === req.params.UserName)

    if (consulta) {
        consulta.userName = req.body.newUsername;
        res.send('Nom d’usuari actualitzat correctament');
    } else {
        res.status(404).send('No s’ha trobat cap usuari amb aquest nom d’usuari');
    }
    
})

app.put('/api/users/changeScore/:UserName', (req, res) => {
    //Canviar la puntuació d’un usuari donat el nom
    var consulta = players.findIndex(a => a.userName === req.params.UserName)

    if (consulta!=-1) {
       
        players[consulta].score = req.body.newScore;
        //consulta.score = req.body.newScore;
        res.send('Score actualitzat correctament');
    } else {
        res.status(404).send('No s’ha trobat cap usuari amb aquest nom d’usuari');
    }
})

app.delete('/api/users/deleteUser/:UserName', (req, res) => {
    // Esborrar un usuari
    var consulta = players.find(a => a.userName === req.params.UserName);
    
    if (consulta != null) {
        const index = players.indexOf(consulta);
        players.splice(index, 1);
        res.send(`Usuari "${consulta.userName}" eliminat correctament`);
    } else {
        res.status(404).send(`No s'ha trobat cap usuari amb el nom d'usuari "${req.params.userName}"`);
    }
});

app.get('/api/ranking', (req,res) => {
    // Obtener ranking
    var sortedPlayers = players.sort((a, b) => b.score - a.score);
    const ranking = sortedPlayers.map((player, index) => ({
        position: index + 1,
        userName: player.userName,
        score: player.score
    }));
    
    res.json(ranking);
});